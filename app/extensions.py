"""Extensions module - Set up for additional libraries goes here."""
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()